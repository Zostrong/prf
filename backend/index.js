const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const expressSession = require('express-session');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

require('./model/user.model');
const userModel = mongoose.model('user');

const dbUrl = 'mongodb://localhost:27017';
mongoose.connect(dbUrl);
mongoose.connection.on('connected', function() {
    console.log('The database connection works');
});

mongoose.connection.on('error', function(error) {
    console.log('Error during the database connection', error);
});



app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors({
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200,
    credentials: true
  }));
  
passport.serializeUser((user, done) => {
    if(!user) return done({msg: "Error - we have no user"}, undefined);
    return done(null, user);
});

passport.deserializeUser((user, done) => {
    if(!user) return done({msg: "Error - no user object to deserialize"}, undefined);
    return done(null, user);
});

passport.use('local', new localStrategy((username, password, done) => {
    userModel.findOne({username: username}, function(err, user) {
        if(err) return done({msg: 'There was an error while retrieving the user'});
        if(user) {
            user.comparePasswords(password, function(error, isMatch) {
                if(error || !isMatch) return done({msg: 'There was an error when comparing the passwords or wrong password'});
                return done(null, user);
            })
        } else {
            return done({msg: 'There is no registered user with that username'});
        }
    })
}));

app.use(expressSession({secret: 'thisisawonderfulunbreakablesecretforourserverapplication'}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', require('./routes'));

const verifyAuthentication = (req, res, next) => {
    if(!req.isAuthenticated()) {
        res.status(403).send({msg: 'You have no right to do that!'});
    } else {
        next();
    }
}

app.use('/user', require('./user_question'));
app.use('/user', require('./user_toplist'));

app.use('/admin', verifyAuthentication);
app.use('/admin', require('./admin_question'));

require('./model/question.model');
const questionModel = mongoose.model('question');

require('./model/toplist.model');
const toplistModel = mongoose.model('toplist');

let rooms = {};
io.on('connection', socket => {

    socket.on('init', () => {
        io.emit("rooms", Object.keys(rooms));
    })

    socket.on("createRoom", ({roomName, username}) => {
        if (rooms[roomName]) {
            io.emit("error", "Room already exists");
        }
        rooms[roomName] = {};
        io.emit("rooms", Object.keys(rooms));
    });

    socket.on("joinRoom", ({roomName, username}) => {
        socket.join(roomName);

        rooms[roomName][username] = 0;
        io.emit("rooms", Object.keys(rooms));
        io.to(roomName).emit("points", rooms[roomName]);

        io.to(roomName).emit("start", {roomName: roomName, size: Object.keys(rooms[roomName]).length});
    })

    socket.on("getQuestion", (roomName) => {
        let size = 1;
        questionModel.aggregate(
        [
            {
            '$sample': { size: size }
            },
            {
            '$project': {
                    answers: {
                        $map: {
                            "input": "$answers",
                            as: "ans",
                            in: {
                                "_id": "$$ans._id",
                                "answer": "$$ans.answer"
                            }
                        }
                    },
                    question: 1
                }
            }
        ])
        .exec(function(err, questions) {
            if(err) io.to(roomName).emit("error", "Getting question failed");
            io.to(roomName).emit("question", questions[0]);
        });
    })

    socket.on("giveAnswer", ({roomName, username, questionId, answerId}) => {
        questionModel.findOne({ _id: questionId }, function(err, question) {
            if(err) io.to(roomName).emit("error", "Processing answer failed");

            let answer = question.answers.find(a => a.correct);
            for (let user of Object.keys(rooms[roomName])) {
                if (username == user) {
                    rooms[roomName][username] += (answer._id == answerId) ? 10 : -5;
                } else {
                    rooms[roomName][username] -= (answer._id == answerId) ? 2 : 0;
                }
            }

            let result = {
                correct: answer['_id'],
                answerId: answerId
            }
            io.to(roomName).emit("answer", result);
            io.to(roomName).emit("points", rooms[roomName])
        });
    })

    socket.on('stopGame', (roomName) => {
        let users = [];
        for (let user of Object.keys(rooms[roomName])) {
            users.push({
                username: user,
                point: rooms[roomName][user]
            })

            toplistModel.create(users, function(error) {
                if(error) io.to(roomName).emit("error", "Saving result failed");
                toplistModel.find({})
                .sort({point: -1})
                .find(function(err, toplist) {
                    if(err) io.to(roomName).emit("error", "Getting toplist failed");
                    io.emit('toplist', toplist);
                    io.emit('start', "")
                }).limit(10);

       
                io.to(roomName).emit("success", "Toplist updated");
            })
        }
        delete rooms[roomName];
        io.emit("rooms", Object.keys(rooms));
        io.to(roomName).emit("start", {roomName: null, size: 0});
    })
});

server.listen(3000, () => {
    console.log('the server is running');
});

/*app.listen(3000, () => {
    console.log('the server is running');
});*/
