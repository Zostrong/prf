const router = require('express').Router();
const mongoose = require('mongoose')

require('./model/question.model');
const questionModel = mongoose.model('question');

require('./model/toplist.model');
const toplistModel = mongoose.model('toplist');

router.route('/question').post((req, res) => {
    if(req.body.username && req.body.questionId) {
        questionModel.findOne({ _id: req.body.questionId }, function(err, question) {
            if(err) return res.status(500).send(err);

            let answer = question.answers.find(a => a.correct);
            return res.status(200).send({correct: answer['_id']});
        });
    } else {
        res.status(400).send({msg: "Felhasználó, kérdés - válasz kötelező"});
    }
});

router.route('/question').get((req, res) => {
    /*let size = req.query.size ? Number(req.query.size) : 5;
    if (size < 1) {
        return res.status(400).send("Méret pozitív kell legyen");
    }*/

    let size = 1;
    questionModel.aggregate(
    [
        {
         '$sample': { size: size }
        },
        {
         '$project': {
                answers: {
                    $map: {
                        "input": "$answers",
                        as: "ans",
                        in: {
                            "_id": "$$ans._id",
                            "answer": "$$ans.answer"
                        }
                    }
                },
                question: 1
            }
        }
    ])
    .exec(function(err, questions) {
        if(err) return res.status(500).send(err);
        return res.status(200).send(questions[0]);
    });
});

module.exports = router;