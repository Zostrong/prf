const mongoose = require('mongoose');

const answerSchema = new mongoose.Schema({
    answer: {type: String, required: true},
    correct: {type: Boolean}
})

const questionSchema = new mongoose.Schema({
    question: {type: String, unique: true, required: true},
    answers: {type: [answerSchema], required: true}
});

mongoose.model('question', questionSchema);