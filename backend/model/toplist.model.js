const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {type: String, required: true},
    point: {type: Number, required: true}
});

mongoose.model('toplist', userSchema);