const router = require('express').Router();
const passport = require('passport');
const mongoose = require('mongoose');
const userModel = mongoose.model('user');

router.route('/login').post((req, res) => {
    if(req.body.username && req.body.password) {
        passport.authenticate('local', (error, user) => {
            if(error) {
                return res.status(403).send(error);
            } else {
                req.logIn(user, (error) => {
                    if(error) return res.status(500).send(error);
                    return res.status(200).send({msg: "Login successful"});
                });
            }
        })(req, res);
    } else {
        res.status(400).send({msg: "Missing username or password"});
    }
});

router.route('/logout').post((req, res) => {
    console.log(req.session.passport.user);
    if(req.isAuthenticated()) {
        req.logout();
        res.status(200).send({msg: "Logout successful"});
    } else {
        res.status(403).send({msg: "Log in, before you log out"})
    }
});

router.route('/register').post((req, res) => {
    if(req.body.username && req.body.password) {
        const user = new userModel({
            username: req.body.username,
            password: req.body.password,
            role: 'default'
        });
        user.save(function(error) {
            if(error) return res.status(500).send(error);
            return res.status(200).send({msg: 'User registered!'});
        })
    } else {
        return res.status(400).send({msg: "Username or password is missing"});
    }
});

module.exports = router;