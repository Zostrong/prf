const router = require('express').Router();
const mongoose = require('mongoose')

require('./model/toplist.model');
const toplistModel = mongoose.model('toplist');

router.route('/toplist').post((req, res) => {
    if(req.body.username && req.body.point) {
        let toplist = new toplistModel({
            username: req.body.username,
            point: req.body.point
        })

        toplist.save(function(error) {
            if(error) return res.status(500).send(error);
            return res.status(200).send({msg: 'Toplistához hozzáadva'});
        })
    } else {
        res.status(400).send({msg: "Felhasználó és pont kötelező"});
    }
});

router.route('/toplist').get((req, res) => {
    toplistModel.find({})
    .sort({point: -1})
    .find(function(err, toplist) {
        if(err) return res.status(500).send(err);
        return res.status(200).send(toplist);
    }).limit(10);
});

module.exports = router;