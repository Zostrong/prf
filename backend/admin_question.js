const router = require('express').Router();
const mongoose = require('mongoose')

require('./model/question.model');
const questionModel = mongoose.model('question');

router.route('/question').post((req, res) => {
    if(req.body.question && req.body.answers && req.body.answers.length == 4) {
        const question = new questionModel({
            question: req.body.question,
        });

        let checkCorrect = false;
        for (let input of req.body.answers) {
            if (!input.answer) {
                res.status(400).send({msg: "Válasz nem lehet üres"});
            }

            if (checkCorrect && input.correct) {
                res.status(400).send({msg: "Több helyes válasz van megadva"});
            }

            if (input.correct) {
                checkCorrect = true;
            }

            question.answers.push({
                answer: input.answer,
                correct: input.correct
            })
        }

        if (!checkCorrect) {
            res.status(400).send({msg: "Helyes válasz kötelező"});
        }
       
        question.save(function(error) {
            if(error) return res.status(500).send(error);
            return res.status(200).send({msg: 'Question added!'});
        })
    } else {
        res.status(400).send({msg: "Kérdés és 4 válasz kötelező"});
    }
});

router.route('/question').get((req, res) => {
    questionModel.find({}, function(err, users) {
        if(err) return res.status(500).send(err);
        return res.status(200).send(users);
    });
});

router.route('/question/:questionId').delete((req, res) => {
    questionModel.deleteOne({ _id: req.params.questionId }, function(err, users) {
        if(err) return res.status(500).send(err);
        return res.status(200).send(users);
    });
});

module.exports = router;