import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../service/user.service';
import { NotificationService } from '../service/notification.service';
import { Subscription } from 'rxjs';
import { Socket, SocketIoModule } from 'ngx-socket-io';

@Component({
  selector: 'app-score-board',
  templateUrl: './score-board.component.html',
  styleUrls: ['./score-board.component.css']
})
export class ScoreBoardComponent implements OnInit {

  displayedColumns: string[] = ['username', 'point'];
  dataSource = [];

  constructor(private socket: Socket,
    private notificationService: NotificationService,
    private userService: UserService) { }

  ngOnInit() {
    this.socket.fromEvent<any>('toplist').subscribe(
      toplist => this.dataSource = toplist
    )

    this.userService.getScoreBoard().subscribe(
      response => this.dataSource = response,
      error => this.notificationService.error(error)
    )
  }

}
