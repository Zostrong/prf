import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuestionService } from '../service/question.service';
import { NotificationService } from '../service/notification.service';
import { MatDialogConfig, MatDialogRef, MatDialog } from '@angular/material';
import { NewQuestionComponent } from '../new-question/new-question.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  displayedColumns: string[] = ['question', 'answerA', 'answerACorrect', 'answerB', 'answerBCorrect',
              'answerCCorrect', 'answerC','answerDCorrect','answerD', 'actions'];
  dataSource = [];

  constructor(private questionService: QuestionService,
    private notificationService: NotificationService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.refreshList();
  }

  addQuestion() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = "85%";
    dialogConfig.width = "60%";
    
    let dialogRef = this.dialog.open(NewQuestionComponent);
    dialogRef.afterClosed().subscribe(response => this.refreshList());
  }

  onDelete(id: number) {
    this.questionService.deleteQuestion(id).subscribe(
      success => {
        this.notificationService.success("Question removed successfully")
        this.refreshList();
      },
      error => this.notificationService.error("Removing question failed")
    )
  }

  refreshList() {
    this.questionService.getAllQuestion().subscribe(
      response => this.dataSource = response,
      error => this.notificationService.error("Getting questions failed")
    )
  }

}
