import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../service/notification.service';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-game-area',
  templateUrl: './game-area.component.html',
  styleUrls: ['./game-area.component.css']
})
export class GameAreaComponent implements OnInit {

  waitingForPlayer: boolean = false;
  constructor(private socket: Socket,
    private notificationService: NotificationService) { }

  ngOnInit() {

    this.socket.fromEvent<string>('error').subscribe(
      error => this.notificationService.error(error)
    )

    this.socket.fromEvent<string>('success').subscribe(
      success => this.notificationService.success(success)
    )

  }

  updateWaitingPlayer($event) {
    this.waitingForPlayer = $event;
  }

}
