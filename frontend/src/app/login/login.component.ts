import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mode = "Change to Admin";
  isGuest: boolean = true;
  errorMessage: string = "";
  form: FormGroup = new FormGroup({
    username: new FormControl('' , Validators.required),
    password: new FormControl('', Validators.required)
  });

  guestForm: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required)
  })

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    localStorage.clear();
  }

  loginGuest() {
    localStorage.setItem("username", this.guestForm.value['username']);
    localStorage.setItem("role", "user");
    this.router.navigate(['/gameArea']);
  }

  changeForm() {
    this.isGuest = !this.isGuest;
    this.mode = this.isGuest ? "Change to Admin" : "Change to Guest";
  }

  login() {
    if (this.form.invalid) {
      return;
    }

    let username = this.form.value['username'];
    let password = this.form.value['password'];

    this.userService.login(username, password).subscribe(
      success => {
        localStorage.setItem("username", username);
        localStorage.setItem("role", "admin");
        this.router.navigate(['/main']);
      },
      error => this.errorMessage = "Bad credentials"
    )
  }

}
