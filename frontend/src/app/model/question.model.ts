import { Answer } from './answer.model';

export class Question {
    _id: number;
    question: string;
    answers: Answer[];
  }
  