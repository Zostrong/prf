
export class Answer {
    _id: string;
    answer: string;
    correct: boolean;
    validated: boolean;
}