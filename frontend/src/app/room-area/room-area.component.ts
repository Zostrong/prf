import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-room-area',
  templateUrl: './room-area.component.html',
  styleUrls: ['./room-area.component.css']
})
export class RoomAreaComponent implements OnInit {

  inGame: boolean = false;
  rooms = this.socket.fromEvent<string[]>('rooms');
  roomName: string = "";

  constructor(private socket: Socket,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.socket.fromEvent<string>('error').subscribe(
      error => this.notificationService.error(error)
    )

    this.socket.fromEvent<any>('start').subscribe(
      response => this.inGame = response.roomName && response.size == 2
    )


    this.socket.emit('init');
  }

  addRoom() {
    let request = {
      roomName: this.roomName,
      username: localStorage.getItem("username")
    }
    
    this.socket.emit('createRoom', request);
    this.roomName = "";
  }

  joinRoom(room: string) {
    let request = {
      roomName: room,
      username: localStorage.getItem("username")
    }

    this.socket.emit('joinRoom', request);
  }

}
