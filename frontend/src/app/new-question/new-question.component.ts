import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { QuestionService } from '../service/question.service';
import { NotificationService } from '../service/notification.service';
import { Question } from '../model/question.model';
import { Answer } from '../model/answer.model';

@Component({
  selector: 'app-new-question',
  templateUrl: './new-question.component.html',
  styleUrls: ['./new-question.component.css']
})
export class NewQuestionComponent implements OnInit {

  public form: FormGroup = new FormGroup({
    question: new FormControl('', Validators.required),
    answerA: new FormControl('', Validators.required),
    answerB: new FormControl('', Validators.required),
    answerC: new FormControl('', Validators.required),
    answerD: new FormControl('', Validators.required),
    answerAcorrect: new FormControl(''),
    answerBcorrect: new FormControl(''),
    answerCcorrect: new FormControl(''),
    answerDcorrect: new FormControl('')
  });

  constructor(private questionService: QuestionService,
    private notificationService: NotificationService,
    private dialogRef: MatDialogRef<NewQuestionComponent>) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    let value = this.form.value;
    let question: Question = new Question();
    question.question = value.question;
    question.answers = [];
    
    let answerA: Answer = new Answer();
    answerA.answer = value.answerA;
    answerA.correct = value.answerAcorrect ? true : false;
    question.answers.push(answerA);

    let answerB: Answer = new Answer();
    answerB.answer = value.answerB;
    answerB.correct = value.answerBcorrect ? true : false;
    question.answers.push(answerB);

    let answerC: Answer = new Answer();
    answerC.answer = value.answerC;
    answerC.correct = value.answerCcorrect ? true : false;
    question.answers.push(answerC);

    let answerD: Answer = new Answer();
    answerD.answer = value.answerD;
    answerD.correct = value.answerDcorrect ? true : false;
    question.answers.push(answerD);

    this.questionService.addQuestion(question).subscribe(
      response => {
        this.notificationService.success("Question saved succesfully");
        this.onClose();
      },
      error => this.notificationService.error(error.msg)
    )

  }

  onClose() {
    this.form.reset();
    this.dialogRef.close();
  }

}
