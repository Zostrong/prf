import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDividerModule} from '@angular/material/divider';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { GameAreaComponent } from './game-area/game-area.component';
import { ScoreBoardComponent } from './score-board/score-board.component';
import { ErrorComponent } from './error/error.component';
import { QuestionAreaComponent } from './question-area/question-area.component';
import { RoomAreaComponent } from './room-area/room-area.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { InterceptorService } from './service/interceptor.service';
import { NewQuestionComponent } from './new-question/new-question.component';
import { MatDialogModule, MatCheckboxModule } from '@angular/material';

const config: SocketIoConfig = { url: 'http://localhost:3000', options: { secure: true }};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    GameAreaComponent,
    ScoreBoardComponent,
    ErrorComponent,
    QuestionAreaComponent,
    RoomAreaComponent,
    NewQuestionComponent
  ],
  imports: [
    SocketIoModule,
    SocketIoModule.forRoot(config),
    MatCheckboxModule,
    MatDialogModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDividerModule,
    MatGridListModule,
    MatSnackBarModule,
    HttpClientModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: InterceptorService, 
      multi: true
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [NewQuestionComponent]
})
export class AppModule { }
