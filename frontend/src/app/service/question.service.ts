import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Question } from '../model/question.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  baseUrl: string = environment.baseUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
   
    })
  };
  
  constructor(private http: HttpClient) { }

  getAllQuestion(): Observable<Question[]> {
    return this.http.get<Question[]>(this.baseUrl + "/admin/question");
  }

  getQuestion(id: number): Observable<Question[]> {
    return this.http.get<Question[]>(this.baseUrl + "/admin/question/" + id);
  }

  addQuestion(question: Question): Observable<any> {
    return this.http.post<any>(this.baseUrl + "/admin/question", question);
  }

  deleteQuestion(id: number): Observable<any> {
    return this.http.delete<any>(this.baseUrl + "/admin/question/" + id);
  }

}
