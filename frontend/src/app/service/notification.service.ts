import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private snackBar: MatSnackBar) { }

  private config: MatSnackBarConfig = {
    duration: 5000,
    verticalPosition: 'top'
  }

  success(msg: string) {
    this.config['panelClass'] = ['success'];
    this.snackBar.open(msg, '', this.config);
  }

  error(msg: string) {
    this.config['panelClass'] = ['error'];
    this.snackBar.open(msg, '', this.config);
  }
}
