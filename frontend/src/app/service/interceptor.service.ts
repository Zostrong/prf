import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import {tap} from 'rxjs/internal/operators';
 
@Injectable()
export class InterceptorService implements HttpInterceptor {
 
    constructor() { }
 
    intercept(request: HttpRequest<any>, next: HttpHandler) {
        request = request.clone({
          withCredentials: true
        });
      return next
        .handle(request)
        .pipe(tap((ev: HttpEvent<any>) => {}));
    }

    
}

