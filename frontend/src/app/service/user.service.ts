import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Toplist } from '../model/toplist.model';
import { Question } from '../model/question.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      })
    };
    return this.http.post<any>(this.baseUrl + "/login", {
      username: username,
      password: password
    }, httpOptions)
  }

  addPoints(item: any): Observable<any> {
    return this.http.post<any>(this.baseUrl + "/user/toplist", item);
  }

  getScoreBoard(): Observable<Toplist[]> {
    return this.http.get<Toplist[]>(this.baseUrl + "/user/toplist");
  }

  getRandomQuestion(): Observable<Question> {
    return this.http.get<Question>(this.baseUrl + "/user/question");
  }

  answerQuestion(item: any): Observable<any> {
    return this.http.post<any>(this.baseUrl + "/user/question", item);
  }
}
