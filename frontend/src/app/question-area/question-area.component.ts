import { Component, OnInit, Input } from '@angular/core';
import { Question } from '../model/question.model';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'app-question-area',
  templateUrl: './question-area.component.html',
  styleUrls: ['./question-area.component.css']
})
export class QuestionAreaComponent implements OnInit {

  points: number = 0;
  answerSelected: boolean = false;

  start: boolean = false;
  pointText: string = "";
  roomName: string = null;
  question: Question;

  constructor(private socket: Socket) { }

  ngOnInit() {
    this.socket.fromEvent<Question>('question').subscribe(
      question => {
        this.question = question;
        this.answerSelected = false;
      }
    )

    this.socket.fromEvent<any>('start').subscribe(
      response => {
        this.roomName = response.roomName;
        if (response.size == 2) {
          this.getQuestion();
        } else {
          this.question = null;
        }
      }
    )

    this.socket.fromEvent<Map<string, number>>('points').subscribe(
      userPoints => {
        this.pointText = "| ";
        for (const [key, value] of Object.entries(userPoints)) { 
          this.pointText += key + " : " + value + " | " 
        }
      }
    )

    this.socket.fromEvent<any>('answer').subscribe(
      success => {
        this.answerSelected = true;

        for (let answer of this.question.answers) {
          if (answer._id == success.answerId || answer._id == success.correct) {
            answer.correct = success.correct == answer._id;
            answer.validated = true;
          }
        }
      }
    )
  }

  getQuestion() {
    this.socket.emit("getQuestion", this.roomName);
  }

  exit() {
    this.answerSelected = false;
    this.socket.emit("stopGame", this.roomName);
  }

  selectAnswer(answerId: string) {
    if (this.answerSelected) {
      return;
    }

    let item = {
      roomName: this.roomName,
      username: localStorage.getItem("username"),
      questionId: this.question._id,
      answerId: answerId
    }

    this.socket.emit("giveAnswer", item);
    this.answerSelected = true;
  }
}
