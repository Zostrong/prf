import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { GameAreaComponent } from './game-area/game-area.component';
import { ErrorComponent } from './error/error.component';
import { AuthGuard } from './service/auth.guard';
import { UserAuthGuard } from './service/user-auth.guard';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  
  {path: 'login', component: LoginComponent},
  {path: 'main', component: MainComponent, canActivate: [AuthGuard]},
  {path: 'gameArea', component: GameAreaComponent, canActivate: [UserAuthGuard]},
  {path: '**', component: ErrorComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
